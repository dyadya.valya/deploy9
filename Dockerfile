FROM python:3.8-buster

WORKDIR /app
COPY . .
ARG USER_GID=$USER_GID
ARG USER_UID=$USER_UID
ARG USERNAME=$USERNAME
ARG T12

ENV T12=$T12
ENV PYTHONPATH="/app"
RUN pip install flask requests redis uwsgi scikit-learn mlflow==1.14.1 protobuf==3.20
CMD uwsgi --http 0.0.0.0:9090 --wsgi-file app.py --master --processes 1 --threads 1

