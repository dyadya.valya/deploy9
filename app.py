import os
import requests
import mlflow
import mlflow.sklearn
from sklearn import datasets
from sklearn.ensemble import RandomForestRegressor
from flask import Flask, jsonify, request, Response

iris = datasets.load_iris()
X = iris.data[:, :2]
y = iris.target

rf = RandomForestRegressor()
rf.fit(X, y)

app = Flask(__name__)

@app.route('/get_source_iris_pred')
def pred1():
    x1 = request.args.get("sepal_length")
    x2 = request.args.get("sepal_width")
    try:
        pred = rf.predict([[x1, x2]])[0]
    except:
        pred = None
    return jsonify({'prediction': pred})

@app.route('/get_string_iris_pred', methods=['GET', 'POST'])
def pred2():
    t12 = os.environ['T12']
    if t12 == 'novalue':
        return Response(status=501)
    else:
        t1, t2 = [float(i) for i in t12.split(',')]
        x1 = request.args.get("sepal_length")
        x2 = request.args.get("sepal_width")
        pred = rf.predict([[x1, x2]])[0]
        if pred < t1:
            pred_class = 'setosa'
        elif t1 < pred < t2:
            pred_class = 'versicolor'
        else:
            pred_class = 'virginica'

        return jsonify({"class": pred,
                        "class_str": pred_class,
                        "threshold_0_1": t1,
                        "threshold_1_2": t2})

if __name__ == "__main__":
    app.run()
else:
    application = app
