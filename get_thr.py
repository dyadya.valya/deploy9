import sys
import json

import urllib.parse
import urllib.request

url = f"http://waryak:5000/get_iris_thresholds"

req = urllib.request.Request(url)
with urllib.request.urlopen(req) as response:
   response = response.read()

response = json.loads(response)
t1 = response['threshold_0_1']
t2 = response['threshold_1_2']

print(f"{t1},{t2}")
